package com.atm.wallace.Model;

public class ATM {

    private int name;
    private int thumbnail;

    public ATM() {
        // Constructor required for Firebase Database
    }

    public ATM(int name, int thumbnail){
        this.name = name;
        this.thumbnail = thumbnail;
    }
    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }
    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}