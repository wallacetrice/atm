package com.atm.wallace.Adapters;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.atm.wallace.Maps.Africa;
import com.atm.wallace.Maps.Centenary;
import com.atm.wallace.Maps.Dfcu;
import com.atm.wallace.Maps.Equity;
import com.atm.wallace.Maps.Orient;
import com.atm.wallace.Maps.Post;
import com.atm.wallace.Maps.Stanbic;
import com.atm.wallace.Maps.Kcb;
import com.atm.wallace.Model.ATM;
import com.atm.wallace.R;
import com.bumptech.glide.Glide;
import java.util.List;

public class AtmAdapter extends RecyclerView.Adapter<AtmAdapter.MyViewHolder> {
    private Context mContext;
    private List<ATM> AtmList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;
        private CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.card_view);
            title = view.findViewById(R.id.title);
            thumbnail = view.findViewById(R.id.thumbnail);
        }
    }

    public AtmAdapter(Context mContext, List<ATM> AtmList) {
        this.mContext = mContext;
        this.AtmList = AtmList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        ATM ATM = AtmList.get(position);
        holder.title.setText(ATM.getName());

        // loading ATM cover using Glide library
        Glide.with(mContext).load(ATM.getThumbnail()).into(holder.thumbnail);

        holder.thumbnail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                switch (position) {
                    case 0:
                       Intent intent = new Intent(mContext, Dfcu.class);
                       mContext.startActivity(intent);
                        break;
                    case 1:
                        mContext.startActivity(new Intent(mContext, Centenary.class));
                        break;
                    case 2:
                        mContext.startActivity(new Intent(mContext, Africa.class));
                        break;
                    case 3:
                        mContext.startActivity(new Intent(mContext, Post.class));
                        break;
                    case 4:
                        mContext.startActivity(new Intent(mContext, Stanbic.class));
                        break;
                    case 5:
                        mContext.startActivity(new Intent(mContext, Equity.class));
                        break;
                    case 6:
                        mContext.startActivity(new Intent(mContext, Orient.class));
                        break;
                    case 7:
                        mContext.startActivity(new Intent(mContext, Kcb.class));
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return AtmList.size();
    }
}
