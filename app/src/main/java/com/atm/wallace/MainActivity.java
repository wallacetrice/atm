package com.atm.wallace;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.ListView;

import com.atm.wallace.Adapters.AtmAdapter;
import com.atm.wallace.Model.ATM;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AtmAdapter adapter;
    private List<ATM> atmList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Banks");

        //listView = findViewById(R.id.listView);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        atmList = new ArrayList<>();
        adapter = new AtmAdapter(this, atmList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareAtms();
        
    }
    
    private void prepareAtms() {
        int[] covers = new int[]{

                R.drawable.dfcu_logo,
                R.drawable.centenary_logo,
                R.drawable.africa_logo,
                R.drawable.postbank_logo,
                R.drawable.stanbic_logo,
                R.drawable.equity_logo,
                R.drawable.orient_logo,
			R.drawable.kcb_logo
        };


        ATM a = new ATM(R.string.dfcu,  covers[0]);
        atmList.add(a);

        a = new ATM(R.string.centenary,  covers[1]);
        atmList.add(a);

        a = new ATM(R.string.africa, covers[2]);
        atmList.add(a);

        a = new ATM(R.string.post, covers[3]);
        atmList.add(a);

        a = new ATM(R.string.stanbic, covers[4]);
        atmList.add(a);

        a = new ATM(R.string.equity,  covers[5]);
        atmList.add(a);

        a = new ATM(R.string.orient, covers[6]);
        atmList.add(a);

        a = new ATM(R.string.kcb, covers[7]);
        atmList.add(a);


        adapter.notifyDataSetChanged();
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx() {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, r.getDisplayMetrics()));
    }
}


