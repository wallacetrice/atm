package com.atm.wallace.Maps;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.atm.wallace.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Timer;
import java.util.TimerTask;

import dmax.dialog.SpotsDialog;


public class Post extends AppCompatActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    AlertDialog waitingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        waitingDialog = new SpotsDialog(Post.this);
        waitingDialog.show();

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                waitingDialog.dismiss(); // when the task active then close the dialog
                t.cancel(); // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 5000); // after 5 second (or 5000 milliseconds), the task will be active.

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(2.7705555, 32.3003457))
                .title("Post bank")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .snippet("Gulu: Main Branch"));

googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(2.7627747, 32.2490263))
                .title("Post bank")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .snippet("Lacor: ATM"));



        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(2.7, 32.1), 10));
    }
}